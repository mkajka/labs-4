﻿using System;
using Lab4.Contract;
using Component1;


namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(Component1.Class1);
        public static Type Component2 = typeof(Component2.Class1);

        public static Type RequiredInterface = typeof(Interface1);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => component2;
        
        #endregion

        #region P2

        public static Type Container = typeof(ComponentFramework.Container);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { };

        public static AreDependenciesResolved ResolvedDependencied = (container) => true;

        #endregion

        #region P3

        public static Type Component2B = typeof(Component2B.Class1);

        #endregion
    }
}
