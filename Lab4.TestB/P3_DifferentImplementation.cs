﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using Lab4.Main;
using System.Reflection;

namespace Lab4.Test
{
    [TestFixture]
    [TestClass]
    public class P3_DifferentImplementation
    {
        [Test]
        [TestMethod]
        public void P3__Component2B_Library_Should_Exist()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P3__Contract_And_Component2_Libraries_Should_Be_Different()
        {
            // Arrange
            var contract = Assembly.GetAssembly(LabDescriptor.RequiredInterface);
            var component2 = Assembly.GetAssembly(LabDescriptor.Component2);
            var component2B = Assembly.GetAssembly(LabDescriptor.Component2B);

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component2B));
            Assert.That(component2, Is.Not.EqualTo(component2B));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Library_Should_Not_Depend_On_Other_Components_Directly()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").EqualTo("Lab4.Contract")
                                        .Or.Property("Name").EqualTo("ComponentFramework")
                                        .Or.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P3__GetInstanceOfRequiredInterface_Shoud_Return_Object_Implementing_RequiredInterface()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            var impl = LabDescriptor.GetInstanceOfRequiredInterface(component);

            // Assert
            Assert.That(impl, Is.InstanceOf(LabDescriptor.RequiredInterface));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = Assembly.GetAssembly(LabDescriptor.Component2B);
            var types = assembly.GetExportedTypes();

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            Assert.That(types, Has.All.EqualTo(LabDescriptor.Component2B));
        }

        [Test]
        [TestMethod]
        public void P3__Component2B_Should_Be_Instantiable()
        {
            // Arrange
            var component = Activator.CreateInstance(LabDescriptor.Component2B);

            // Assert
            Assert.That(component, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Dependencies_Should_Be_Resolved_After_Registering_Component1_And_Component2B()
        {
            // Arrange
            var container = Activator.CreateInstance(LabDescriptor.Container);
            var component1 = Activator.CreateInstance(LabDescriptor.Component1);
            var component2B = Activator.CreateInstance(LabDescriptor.Component2B);

            // Act
            LabDescriptor.RegisterComponent(container, component1);
            LabDescriptor.RegisterComponent(container, component2B);

            // Assert
            Assert.That(LabDescriptor.ResolvedDependencied(container));
        }
    }
}
